# Sound Splitter

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

Python Tool for splitting audio and video files into separate files.

The tool levers a JSON file to indicate how to split up the input file. Why a JSON file you may ask, isn't that a little overkill? Sure for a simple single file split, but what if you want to split the source into 10 different files? This makes that easy. How about split out a half dozen sound bytes for a podcast? Piece of cake. Grab 3 or 4 clips from a movie? No problem. How about split a single file live concert into tracks? Easy Peasy.

---

## Setup

You will need to have FFMpeg installed

### FFMpeg for Debian type Linux

Install FFmpeg if not already installed.

```sh
sudo apt install ffmpeg
```

### FFMpeg for Windows

Download the latest version from [this link](https://www.gyan.dev/ffmpeg/builds/). Unzip the file to somewhere permenant and then link `%PATH_TO_FILES%/bin` to Windows PATH. This is required to use FFmpeg in the CLI.

### Python Requirements

```sh
pip install -r requirements.txt
```

---

## Usage

The app should be run by invoking it as a python file. `py av-split.py` for Windows or `python3 av-split.py` for the Linux / Mac world.

The following arguments may be parsed to the app:

| Argument | Task |
| -------- | ---- |
| `-h, --help` | Show help |
| `-g, --generate-sample` | Generate a example or sample JSON file which can be used as a template when running the app. |
| `-p JSON_PATH, --json-path JSON_PATH` | The path of the JSON file used in the execution of AV splitting. Required in the usual run of things. |
| `-v, --verbose` | Show all the FFMpeg feedback text. |
| `-o, --overwrite-existing` | Cause the app to overwrite an existing file of the same name. |

### Examples

The first thing you should do is to generate the JSON file to parse back to the app for splitting. This is done as follows:

```sh
py av-split.py -g
```

This will create a file called `sample.json`, which can be renamed as desired. For the purpose of these examples we will leave it named `sample.json`.

Next you will need to edit this file to set it up the desired input and output. The default looks like this:

```json
{
    "file": "PATH_TO_ORIGONAL_FILE",
    "output": "mp3",
    "parts": {
        "NEW_FILE_NAME": {
            "start_time": "HH:MM:SS",
            "end_time": "HH:MM:SS"
        }
    }
}
```

- `"file"`: The name of the file you wish to split. If the file is not in the current working directory, you can specify a full or relative path.
- `"output"`: the format to convert to. Options include mp3 & mp4.
- `"parts"`: The different tracks or clips desired. At least one must be defined, but you may define as many as you like. Each one will result in a seperate clip or track.
  - `"NEW_FILE_NAME"`: Name for the new track or clip.
  - `"start_time"`: The timestamp in the origonal clip from which to start.
  - `"end_time"`: The timestamp in the origonal clip from which to end.
  - `"duration"`: Alternative to `end_time`. Define how long the clip should be. If both `end_time` and `duration` is defined, `duration` will be ignored.

So your edited JSON file may look something like this:

```json
{
    "file": "example.mp4",
    "output": ".mp3",
    "parts": {
        "part01": {
            "start_time": "00:05:00",
            "end_time": "00:06:20"
        },
        "part02": {
            "start_time": "00:06:00",
            "duration": "00:02:00"
        }
    }
}
```

Finally execute the app again to perform your split:

```sh
py av-split.py -p sample.json
```

The app will split your files as defined.

---
