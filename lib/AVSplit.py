class AVSplit:

    def __init__(self, data: dict, params = {'verbose': False, 'overwrite': True}) -> None:
        self.data = data
        self.check_required_data()
        self.verbose = params['verbose']
        self.overwrite = params['overwrite']


    def check_required_data(self) -> None:
        """Check through the JSON data and validate it's all correct.

        If not, terminate gracefully.
        """
        if type(self.data) is not dict:
            self.throw_data_error('Invalid data. Please check your JSON file.')
        from lib.functions import file_exists
        if file_exists(self.data['file']) == False:
            self.throw_data_error("Source file " + self.data['file'] + " cannot be found.")
        required_keys = ['file', 'output', 'parts']
        for key in required_keys:
            if key not in self.data.keys():
                self.throw_data_error('Required key missing in JSON file: ' + key)
        if len(self.data['parts']) == 0:
            self.throw_data_error('At least one part has to be defined')
        from lib.functions import validate_timestamp
        for file_name in self.data['parts']:
            if type(self.data['parts'][file_name]) is not dict:
                self.throw_data_error('Invalid data near ' + file_name + '. Please check your JSON file.')
            if 'start_time' not in self.data['parts'][file_name].keys():
                self.throw_data_error('Required key missing in JSON file near ' + file_name + ': start_time')
            if 'end_time' not in self.data['parts'][file_name].keys() and 'duration' not in self.data['parts'][file_name].keys():
                self.throw_data_error('Required key missing in JSON file near ' + file_name + ': either "end_time" or "duration"')
            if validate_timestamp(self.data['parts'][file_name]['start_time']) == False:
                self.throw_data_error('Invalid start time for ' + file_name)
            if 'end_time' in self.data['parts'][file_name].keys():
                if validate_timestamp(self.data['parts'][file_name]['end_time']) == False:
                    self.throw_data_error('Invalid end time for ' + file_name)
            else:
                if validate_timestamp(self.data['parts'][file_name]['duration']) == False:
                    self.throw_data_error('Invalid duration for ' + file_name)


    def throw_data_error(self, txt: str) -> None:
        """Display an error and halt the execution of the program.

        Args:
            txt (str): The text to display to the user.
        """
        print(txt)
        exit()


    def split(self) -> None:
        """Perform the split.
        """
        import ffmpy
        from progress.bar import Bar

        print("Splitting " + self.data['file'] + " into " + str(len(self.data['parts'])) + " parts.")
        with Bar('Progress', max=len(self.data['parts'])) as bar:
            for part in self.data['parts']:
                if self.data['output'][0] != ".":
                    self.data['output'] = '.' + self.data['output']
                output_file = part + self.data['output']
                params = '-ss ' + self.data['parts'][part]['start_time']
                if 'end_time' in self.data['parts'][part].keys():
                    params += " -to " + self.data['parts'][part]['end_time']
                else:
                    params += " -t " + self.data['parts'][part]['duration']

                if self.verbose == False:
                    params += ' -hide_banner -loglevel error'
                if self.overwrite == True:
                    params += ' -y'
                else:
                    params += ' -n'

                ff = ffmpy.FFmpeg(
                    inputs={self.data['file']: None},
                    outputs={output_file: params}
                )
                try:
                    ff.run()
                    bar.next()
                except ffmpy.FFRuntimeError:
                    self.throw_data_error("Unable to split file.")
        print("Split complete")
